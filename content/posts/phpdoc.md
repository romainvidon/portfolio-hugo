+++
title = "Documenter son code PHP avec PHPDoc"
date = "2022-05-02T19:22:39+02:00"
author = "Romain"
authorTwitter = "romainvidon" #do not include @
cover = ""
tags = ["veille", "php", "documentation"]
keywords = ["veille", "php", "documentation"]
description = "Comment bien documenter son code"
showFullContent = false
readingTime = false
hideComments = false
showTableOfContents =  true
type = "post"
+++

PHPDoc est une convention de documentation pour PHP, basée sur JavaDoc. Elle permet de documenter son code via des commentaires spéciaux, puis de générer une documentation avec entres autres Doxygen ou phpDocumentor.
Certains éditeurs de texte peuvent également utiliser PHPDoc pour afficher plus d’informations sur les fonctions : VSCode, NetBeans, PhpStorm … L’IDE peut également typer plus facilement les variables, et afficher d’éventuelles erreurs.

## Écrire un DocBlock
Un DocBlock est un commentaire multi-ligne qui contient des infos sur l'entité  en dessous grâce aux tags.
Il est possible de l’appliquer sur un fichier entier, une méthode, une interface, une fonction, une classe ou encore une variable.

La première ligne contient un **résumé** de la fonction. Ensuite il est possible de mettre une **description détaillée**, sur plusieurs lignes. Enfin viennent les **tags**, contenant des informations spécifiques.

Le texte peut être mis en forme avec Markdown.

Un exemple de fonction documentée :
```php
/**
 * Effectue une multiplication
 * 
 * La foncton **multiplie** deux nombres ensembles.
 * @param float $a Le *premier* nombre à multiplier
 * @param float $b Le *second* nombre à multiplier
 * @return float Le *produit* des 2 nombres
 */  
function multiplier(float $a, float $b): float {
  return $a * $b;
}
```

## Tags principaux
*Note : `<argument>` correspond à un argument, `[<argument>]` entre crochets signifie qu'il est optionnel.*

### Pour toutes les entités
- `@author <nom> [<email>]` : l’auteur·e de l'entité. Si l'email est présent, il doit être entre chevrons.

  *Exemple : `@author My Name <my.name@example.com>)`*

- `@todo <description>` : indiquer des tâches à réaliser
- `@deprecated [<description>]` : définir l'entité comme obsolète, ajouter si possible un `@see` pour définir un remplacement
- `@see <URL OU structure> [<description>] ` : faire une référence à une autre entité ou à une URL
  
  *Exemple : `@see https://dagrs.berkeley.edu/sites/default/files/2020-01/sample.pdf`*

### Fonctions et méthodes
- `@param <type> <$variable> [<description>]` : définir un paramètre
  
  *Exemple : `@param string $nom Le nom de l'animal`*

- `@return <type> [<description>]` : décrit la valeur retournée par la fonction
  
  *Exemple : `@param float La valeur calculée`*


- `@throws <type> [<description>]` : décrit une erreur pouvant être renvoyé par la fonction, et entre autres dans quel contexte peut-elle être envoyée.
  
  *Exemple : `@return TypeError L'argument fourni n'est pas un nombre`*

## Les types

PHPDoc utilise les mêmes types de PHP. Vous pouvez donc utiliser les types primitifs, des tableaux (par exemple `int[]` pour un tableau d'entiers), ou des classes. Il est aussi possible d'utiliser des types d'union commme `string|int` depuis PHP 8.

## Générer une documentation avec Doxygen
**Doxygen** permet de générer une documentation à partir d'un projet, et de l'exporter sous différents formats comme HTML ou LaTeX.

L'application **Doxywizard** permet de générer un fichier de configuration **Doxyfile** de manière graphique. L'interface simplifiée est séparée en 4 parties :
- **Project** avec la définition du nom et l'icône du projet, le dossiers des sources (cocher "Scan Recursively" pour parcourir les sous-dossiers) et le dossier de destination des documentations
- **Mode** pour définir le mode de traitement : sélectionner toutes les entités ou seulement les entités documentées, et comment optimiser l'affichage (ici pour PHP)
- **Output** pour choisir les formats de sortie : la documentation peut être exportée entre autres en HTML, LaTeX ou encore en pages pour la commande `man`.
- **Diagrams** pour générer des diagrammes de relations de classes

L'interface experte, disponible depuis l'onglet `Expert`, permet de configurer chaque option disponible avec une description pour chaque champ.

Une fois les réglages terminés, vous pouvez exécuter Doxygen en vous rendant dans l'onglet `Run`, puis en cliquant sur `Run doxygen`.

Si tout se passe correctement, la documentation sera générée dans le dossier de destination.

Il est également possible de compiler la documentation via la console, avec `doxygen <fichier de configuration>`. Par défaut, doxygen utilise le fichier **Doxyfile**.

## Liens
- Documentation de PHPDoc <https://docs.phpdoc.org/3.0/guide/references/phpdoc/index.html>
- Liste des tags <https://docs.phpdoc.org/3.0/guide/references/phpdoc/tags/index.html#tag-reference> 
- Liste des types primaires de PHP <https://www.php.net/manual/fr/language.types.declarations.php>
- Manuel de Doxygen <https://www.doxygen.nl/manual/config.html>
- Une référence de Markdown <https://www.markdownguide.org/basic-syntax/>