+++
title = "Installer facilement ses logiciels sur Windows avec winget"
date = "2022-04-29T11:59:46+02:00"
author = "Romain"
authorTwitter = "romainvidon" #do not include @
cover = ""
tags = ["veille", "windows"]
keywords = ["veille", "windows", "winget"]
description = "Aperçu rapide du gesionnaire de paquets de Windows"
showFullContent = false
readingTime = false
hideComments = true
showTableOfContents =  true
type = "post"
+++

Il existe depuis longtemps des gestionnaires de paquets sur Windows qui proposent d’installer des logiciels depuis un dépôt central, comme [Chocolatey](https://chocolatey.org/). Microsoft propose désormais son propre gestionnaire, winget.

L’application winget est pré-installée sur Windows 11 et Windows 10 à partir de la mise à jour 1803

## Utilisation de Winget
Ouvrez une invite de commandes ou un terminal PowerShell, et saisissez `winget`. Les commandes disponibles vont s’afficher.
Les principales commandes :
- `install <paquet>` : installer le paquet
- `uninstall <paquet>` désinstalle le paquet
- `upgrade <paquet>` : met à jour le paquet.
    - Sans argument, affiche la liste des paquets pouvant être mis à jour.
    - `-all` permet de tout mettre à jour.
- `list` : affiche la liste des paquets installés
- `show <paquet>` : affiche les infos sur le paquet.
- `export -o <destination>` : exporte un fichier JSON avec la liste des paquets installés (voir https://github.com/microsoft/winget-cli/blob/master/doc/windows/package-manager/winget/export.md )
	- `--include-versions` : inclure les versions
- `import -i <fichier>` : importe un fichier précédemment exporté.
	- `--ignore-unavailable` permet d’ignorer les paquets non disponibles
	- `--ignore-versions` permet d’ignorer les versions
Il est également possible, voire parfois nécessaire, de désigner un paquet via son identifiant, avec le paramètre --id <id>, par exemple --id VideoLAN.VLC

## Quelques packages intéressants
- [LibreOffice](https://fr.libreoffice.org/) (suite bureautique) `winget install libreoffice`
- [VLC](https://www.videolan.org/vlc/) (lecteur multimédia) `winget install --id VideoLAN.VLC`
- Mozilla Firefox (navigateur Web) `winget install --id Mozilla.Firefox`
- Google Chrome (navigateur Web) `winget install --id Google.Chrome`
- Chromium (navigateur web, partie libre, open source et sans mouchards de Google Chrome) `winget install --id Hibbiki.Chromium`
- Mozilla Thunderbird (client de messagerie) `winget install --id Mozilla.Thunderbird`
- Git (gestionnaire de version) `winget install --id Git.Git`
- VSCodium (éditeur de texte personnalisable, version libre, open source et sans mouchards de Visual Studio Code) `winget install --id VSCodium.VSCodium`
- KeePassXC (gestionnaire de mots de passe) `winget install keepassxc`
- Meld (visualiseur de différences de fichiers, avec support Git) `winget install --id Meld.Meld`
- GIMP (édition d’image) `winget install --id GIMP.GIMP`

## Liens
- La page GitHub de winget https://github.com/microsoft/winget-cli
- Winget.run, un catalogue en ligne des programmes https://winget.run/
