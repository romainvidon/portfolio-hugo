---
title: "Portfolio"
date: 2021-11-08T14:31:27+01:00
tags: ["project","web"]
draft: false
type: "post"
---

Réalisation de ce portfolio grâce au framework de sites statiques [Hugo](https://gohugo.io/).

[Code source](https://gitlab.com/romainvidon/portfolio-hugo)