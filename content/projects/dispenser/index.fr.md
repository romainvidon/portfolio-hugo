---
title: "Distributeur connecté de croquettes"
date: 2018-05-25
tags: ["project","school","node"]
description: "Projet réalisé pour le baccalauréat STI2D"
type: "post"
draft: false
---

**Rôle dans l'équipe** : développeur 

**Technologies utilisées** : Node.js (serveur), AppInventor (application mobile)

Dans le cadre de mon baccalauréat STI2D, j'ai dû réaliser un projet en groupe, nous avons chosi de réaliser un distributeur de croquettes, avec une distribution programmable via un téléphone.

{{<figure src="dispenser.png" alt="Modélisation du distributeur" caption="Modélisation du distributeur sur SolidWorks">}}

Notre groupe était constitué de 2 sous-groupes, qui communiquaient régulièrement :
- un groupe s'occupant de la modélisation du système et du mécanisme de distribution,
- un groupe gérant l'électronique et le développement des applications.

Lors de ce projet, j'ai principalement réalisé les applications serveur et mobile.

## Application mobile
L'application mobile a été réalisée sur AppInventor. Elle permet de définir les différents horaires.

{{<figure src="app.png" alt="Capture d'écran de l'application : menu des paramètres" caption="Menu des paramètres de l'application" style="width: 50%; margin: auto;">}}

## Application serveur
Le serveur consiste en une application Node.js hébergée sur une Raspberry Pi.

[Code source du serveur](https://gitlab.com/-/snippets/2202492)


{{< youtube id="o0N48BVHAMM" title="Versement d\'une dose de croquettes">}}

## Compétences acquises

L'objectif étant de créer un système, j'ai pu participer à sa construction, de son idée jusqu'au prototype. J'ai également découvert le fonctionnement de Node.js et de Linux, grâce à la Raspberry Pi.