---
title: "Links"
draft: false
---

**Twitter** [@romainvidon](https://twitter.com/romainvidon)

**LinkedIn** [@romainvidon](https://www.linkedin.com/in/romainvidon/)

**GitHub** [@romainvidon](https://github.com/romainvidon)

**GitLab** [@romainvidon](https://gitlab.com/romainvidon)