---
title: "Liens / Contact"
description: "Développeur web"
draft: false
---

**Email** [romain [point] vidon76 [arobase] gmail [point] com](&#109;&#97;&#105;&#108;&#116;&#111;&#58;%72%6F%6D%61%69%6E%2E%76%69%64%6F%6E%37%36%40%67%6D%61%69%6C%2E%63%6F%6D)

**Twitter** [@romainvidon](https://twitter.com/romainvidon)

**LinkedIn** [@romainvidon](https://www.linkedin.com/in/romainvidon/)

**GitHub** [@romainvidon](https://github.com/romainvidon)

**GitLab** [@romainvidon](https://gitlab.com/romainvidon)