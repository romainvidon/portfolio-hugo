+++
title = "À propos"
toc = true
draft = false
+++

{{<link-button href="/CV_VIDON_Romain_Développeur_Web_Back-end.pdf" text="Voir mon CV" newTab=true >}}

## Expériences professionelles

{{<timeline>}}
{{% event title="Ingénieur d'études" start="2022-10-04" startFormat="octobre 2022" end="2022-04-01" endFormat="avril 2023" info="Niji, Lyon"%}}

En contrat pour Enedis sur Cinke-Évolution, logiciel de gestion des techniciens, de leurs plannings et de leurs ressources.

#### Technologies utilisées

- Microservices **Spring**
- Front-end avec **Angular**
- Base de données **PostgreSQL**, accès depuis les services avec **jOOQ** et **MapStruct**
- Tests unitaires avec **JUnit** et E2E avec Cypress et Protractor
{{% /event %}}

{{% event title="Développeur Web" start="2022-04-04" startFormat="avril 2022" end="2022-06-03" endFormat="juin 2022" info="GRIFC, Préaux"%}}

Mise à jour et maintenance d'Inauto, progiciel de gestion de pièces automobile

#### Technologies utilisées

- application serveur en **PHP**
- utilisation de bases de données **Oracle**
- interface utilisateur avec **Bootstrap**
{{% /event %}}

{{% event title="Développeur Web fullstack" start="2021-04-04" startFormat="avril 2021" end="2021-07-04" endFormat="juillet 2021" info="Webky, Rouen (stage)"%}}
Réalisation de divers projets clients et internes :
- envoi de notifications par mail et SMS via l'API OVH
- utilisation de l'API de Dolibarr pour générer des factures pour un logiciel de billetterie
- API de signature électronique de contrat

#### Technologies utilisées

- **Node.js** pour l'application serveur
- SGBD **MariaDB**
- ORM **Bookshelf.js**
- **RabbitMQ** pour gérer l'envoi en masse de mails
{{% /event %}}

{{% event title="Développeur Web fullstack" start="2020-01-13" startFormat="janvier 2020" end="2020-02-14" endFormat="février 2020" info="Webky, Rouen (stage)"%}}
Mise à jour du site de la Cartoon Fair
- affichage des invités et des exposants via une base de données
- mise à jour de la structure de la base de données
- intégration d'un module de planning

#### Technologies utilisées

- **Node.js** pour l'application serveur
- **Express.js** pour le routage
{{% /event %}}

{{% event title="Développeur Web" start="2019-05-13" startFormat="mai 2019" end="2019-06-14" endFormat="juin 2019" info="Abécédaire, Rouen (stage)"%}}
Réalisation d'une extension pour navigateur de choix de boîte mail lors d'un clic sur une adresse électronique.

#### Technologies utilisées

- **JavaScript** avec l'API **WebExtensions**
- **Bootstrap** pour l'interface utilisateur
{{% /event %}}
{{</timeline>}}


---

## Formations

{{<timeline>}}
{{% event title="Licence Professionnelle - Métiers de l'informatique : conduite de projet - Technologie du développement web" startFormat="septembre 2020" endFormat="juillet 2021" info="Université de Limoges, formation à distance"%}}

- Gestion de projets : méthode Scrum
- Utilisation des frameworks Angular et Symfony
- Bases de données NoSQL avec MongoDB, création d'une API REST avec Feathers.js
- Notions de sécurité à travers l'utilisation de Laravel
{{% /event %}}

{{% event startFormat="octobre 2021" endFormat="décembre 2021" title="MOOC Gestion de projets" info="https://gestiondeprojet.pm" %}}

- Apprentissage des différentes notions de gestion de projet
- Gestion des risques

[Voir l'attestation de réussite](https://certification.gestiondeprojet.pm/GdP16A/GdP16a-LPUGppYGC.pdf)
{{% /event %}}


{{% event title="BTS Services Informatiques aux Organisations" startFormat="septembre 2018" endFormat="juillet 2020" info="Lycée Gustave Flaubert, Rouen"%}}
- Programmation orientée objet : C#, PHP
- Gestion de bases de données MySQL
- Économie et droit de l'informatique
{{% /event%}}

{{% event title="Bac Sciences et Techniques de l'Industrie et du Développement Durable (STI2D)" startFormat="septembre 2016" endFormat="juillet 2018" info="Lycée Blaise Pascal, Rouen"%}}
- Étude de systèmes
- Électronique
- Projet : distributeur connecté de croquettes (Raspberry Pi avec Node.js) 

{{<link-button text="En savoir plus de détails sur le distributeur" link="/projects/dispenser/" >}}
{{% /event %}}

{{</timeline>}}